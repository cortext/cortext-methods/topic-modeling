######## example_param.init ######################################
# Exemple de fichier décrivant les paramètres d'un script
# (afin de générer le formulaire dans le ctManager),
# et ses entrées/sorties (afin de le conecter à d'autres scripts)
# Type de paramètres : list, text, int, string, (...)
# Ce fichier est à renommer en "nom_du_script.yml" et à mettre dans le répertoire du script
###########################################################

##
############## Description du script lui-même pour l'utilisateur de ctManager, ainsi que les formats d'entrée/sorties ############################
##

script:
  name: Topic Modeling
  desc: "Produce Topics using Gensim LDA"
  type: Corpus Clustering
  author: jp cointet
  documentation: https://docs.cortext.net/analyzing-data/topic-modeling/
  tags: [clustering]
  inputs:
   -
      type: sqlite
      structure: reseaulu      
  outputs:
    -
      type: sqlite
      categ: biblio
      structure: reseaulu
    # -
    #   type: sqlite
    #   categ: biblio
    #   structure: records_bdd


##
############## Paramètres du scripts (construction du formulaire utilisateur et variables renvoyées aux script) ############
##
section:
  sect1:
    label: Data Description
    order: 1
    required: yes
    opened: yes
    params:
      container:
        label: Container to be used
        help: Special parameter removed before instantiating CortextMethod
        required: yes
        type: list
        widget:
          choices: ["docker:cortext-methods/topic-modeling"]
          default: "docker:cortext-methods/topic-modeling"
      fields:
        label: Fields
        help: Choose the fields you want to index
        type: list
        required: yes
        widget:
          source: descriptor
          multiple: yes
          expanded: yes
          choices: textual_fields
          default: ['title','abstract']
      topic_number:
        label: Number of Topics - (0 for automatic search)
        help: Number of topics of the LDA model (max is limited to 200) 
        type: string
        required: no
        widget:
          source: no
          default: 10
          hide: no
      froms:
        label: "Minimum number of topics"
        parent: topic_number
        value: 0
        help: "Min number of topics"
        required: no
        type: string
        required: yes
        widget:
          source: no
          default: '10'
          hide: no
      tos:
        label: "Maximum number of topics"
        parent: topic_number
        value: 0
        help: "Max number of topics (limit is set to 200)"
        required: no
        type: string
        required: yes
        widget:
          source: no
          default: '40'
          hide: no
      steps:
        label: "Steps"
        parent: topic_number
        value: 0
        help: "Steps in between two successive topic numbers"
        required: no
        type: string
        required: yes
        widget:
          source: no
          default: '10'
          hide: no
      customname:
        label: Custom name for storing topics
        help: Indicate a custom name to identify the resulting variable
        type: string
        required: no
        widget:
          source: no
          default: ''
          hide: no
      max_topics_per_document:
        label: Maximum number of topics per document
        help: Only the n most prevalent topic per document will be stored
        type: string
        required: no
        widget:
          source: no
          default: '3'
          hide: no

  sect2:
    label: Text Cleaning Parameters
    order: 2
    required: yes
    opened: yes
    params:
      lowercase:
        label: Lower Case
        help: Lower Case
        required: yes
        type: list
        widget:
          source: list
          multiple: no
          expanded: yes
          choices: [yes,no]
          default: yes
      language:
        label: language
        help: language (only useful if stemming or/and stop word removal is active)
        required: yes
        type: list
        widget:
          source: list
          multiple: no
          expanded: yes
          choices: ['arabic', 'danish' ,'dutch' ,'english', 'finnish', 'french' ,'german', 'hungarian','italian' ,'norwegian' , 'portuguese' ,'romanian' ,'russian', 'spanish', 'swedish']
          default: 'english'
      stopwordremoval:
        label: Stop-words Removal
        help: Only working if the original language you are working with is listed
        required: yes
        type: list
        widget:
          source: list
          multiple: no
          expanded: yes
          choices: [yes,no]
          default: yes
      punctuationfree:
        label: Remove punctuation
        help: Remove punctuation
        required: yes
        type: list
        widget:
          source: list
          multiple: no
          expanded: yes
          choices: [yes,no]
          default: yes
      lemmatize:
        label: Stemming
        help: Only working if the original language you are working with is listed
        required: yes
        type: list
        widget:
          source: list
          multiple: no
          expanded: yes
          choices: [yes,no]
          default: yes

      doc_threshold:
        label: Minimum frequency of words
        help: Minimum frequency of words in the vocabulary
        type: string
        required: no
        widget:
          source: no
          default: 5
          hide: no
      no_above:
        label: Maximum frequency of words (in percentage of the total corpus) 
        help: Words appearing in a greater number of documents than this proportion will be discarded
        type: string
        required: no
        widget:
          source: no
          default: "50"
          hide: no
  sect3:
    label: LDA algorithm parameters
    order: 3
    required: yes
    opened: yes
    params:
      alpha:
        label: Alpha
        help: Hyperparameters that affect sparsity of the document-topic (see LDA gensim documentation)
        type: string
        type: list
        widget:
          source: list
          multiple: no
          expanded: yes
          choices: ['symmetric','asymmetric','auto']
          default: 'symmetric'
      iterations:
        label: Number of iterations for learning the model
        help: Number of iterations for learning the model
        type: string
        required: no
        widget:
          source: no
          default: 20
          hide: no
      # clusterthreshold:
      #   label: Doc 2 topic minimum probability
      #   help: Will assign a topic to any document if its probability is higher than this value
      #   type: string
      #   required: no
      #   widget:
      #     source: no
      #     default: ".15"
      #     hide: no
