#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cortextlib.legacy import containerize

import os, sys

reload(sys) 


import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

sys.setdefaultencoding("utf-8")
from sqlite3 import *
import types
import re
from librarypy.path import *
from librarypy import fonctions
import csv, codecs, cStringIO
import random
import shutil
from nltk.corpus import stopwords 
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import SnowballStemmer
import string
import gensim
from gensim import corpora
import pyLDAvis.gensim
from operator import itemgetter
import numpy as np
np.warnings.filterwarnings('ignore')

import logging,pprint

try:
	print user_parameters
except:
	user_parameters=''

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from gensim.models import CoherenceModel

plt.ioff()


parameters_user=fonctions.load_parameters(user_parameters)


corpus_file = parameters_user.get('corpus_file','')
result_path=parameters_user.get('result_path','')
result_path0=result_path[:]
fonctions.progress(result_path0,0)

fields=parameters_user.get('fields',['CO'])
topic_number=parameters_user.get('topic_number','10')
iterations=parameters_user.get('iterations','20')
doc_threshold=parameters_user.get('doc_threshold','5')
#clusterthreshold=parameters_user.get('clusterthreshold','0.15')
custom_name=parameters_user.get('custom_name','')

language=parameters_user.get('language','english')

lowercase=parameters_user.get('lowercase',True)
stopwordremoval=parameters_user.get('stopwordremoval',True)
punctuationfree=parameters_user.get('punctuationfree',True)
lemmatize=parameters_user.get('lemmatize',False)
alpha=parameters_user.get('alpha','symmetric')
try:
	no_above=float(parameters_user.get('no_above',50))/100.
except:
	logging.debug('You must enter a valid proportion (int value between 0 and 100)')
	dqsdq
customname=parameters_user.get('customname',"").lower()
customname=customname.replace(' ','_').replace('é','e').replace('é','e').replace('è','e').replace('.','').replace('-','_')


################################
##logging user parameters#######
################################
logging.basicConfig(filename=os.path.join(result_path,'.user.log'), filemode='w', level=logging.DEBUG,format='%(asctime)s %(levelname)s : %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logging.info('Script Topic Modelling Started')
yamlfile = 'topic_modeling.yaml'
if 'cointet' in os.getcwd():
	yamlfile = '/Users/jpcointet/Desktop/cortext-methods/topic_modeling/'+yamlfile
if 1:
	parameterslog=fonctions.getuserparameters(yamlfile,parameters_user)
	logging.info(parameterslog)
else:
	pass
logger = logging.getLogger()
logging.info(parameterslog)
logger.handlers[0].flush()
fonctions.progress(result_path0,1)


try:
	iterations=int(iterations)
except:
	logging.debug('You must enter a valid number of iterations (integer value)')
	dqsdq
try:
	topic_number=min(200,int(topic_number))
except:
	logging.debug('You must enter a valid number of topics (integer value)')
	dqsdq
try:
	doc_threshold=int(doc_threshold)
except:
	logging.debug('You must enter a valid number of topics (integer value)')
	dqsdsq
try:
	#print 'clusterthreshold',clusterthreshold
	clusterthreshold=float(1/float(topic_number-.5))

except:
	logging.debug('You must enter a valide number of topics (integer value)')
	dqsdsq






from librarypy import descriptor
try:
	print 'here we are'
	from librarypy import descriptor
	data=descriptor.get_descriptor(corpus_file)
	print 'data',data
	corpus_type=data['corpus_type']
except:
	corpus_type=parameters_user.get('corpus_type','other')



dico_champs={}
if corpus_type=='isi':
	dico_champs['Keywords'] = 'ISIkeyword'
	dico_champs['Cited Author'] = 'ISICRAuthor'
	dico_champs['Cited Journal'] = 'ISICRJourn'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['References'] = 'ISIRef'
	dico_champs['Research Institutions'] = 'ISIC1Inst'
	dico_champs['Subject Category'] = 'ISISC'
	dico_champs['Countries'] = 'ISIC1Country'
	dico_champs['Journal'] = 'ISIJOURNAL'
	dico_champs['Journal (long)'] = 'ISISO'
	dico_champs['Cities'] = 'ISIC1City'
	dico_champs['Author'] = 'ISIAUTHOR'
	dico_champs['Terms'] = 'ISIterms'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['cp'] = 'cp'
	dico_champs['Country'] = 'Country'
	dico_champs['Year'] = 'ISIpubdate'
	dico_champs['Periods'] = 'ISIpubdate_custom'
	dico_champs['Publication Type'] = 'ISIDT'
	dico_champs['WOS Category'] = 'ISIWC'
	dico_champs['Funding'] = 'ISIFU'
	dico_champs['Keywords']='ISIkeyword'

	dico_champs['Abstract']='ISIABSTRACT'
	dico_champs['Title']='ISITITLE'
	dico_champs['Acknowledgement']='ISIFX'


dico_champs['Terms']='ISIterms'
dico_champs['Periods'] = 'ISIpubdate_custom'
if corpus_type=='xmlpubmed' or corpus_type=='isi' :
	dico_champs['Year'] = 'ISIpubdate'
else:
	dico_champs['Time Steps'] = 'ISIpubdate'


bdd_name = corpus_file

###########################
#ACTUAL CODE STARTING HERE#
###########################

fonctions.check_bddname(bdd_name)
filename_v = fonctions.get_data(bdd_name,'ISIpubdate',limit_id=1)
try:
	filename=filename_v.values()[0][0]['file']
except:
	filename='f'

###first compile the data###

#doc1 = "Sugar is bad to consume. My sister likes to have sugar, but not my father."
# doc2 = "My father spends a lot of time driving my sister around to dance practice."
# doc3 = "Doctors suggest that driving may cause increased stress and blood pressure."
# doc4 = "Sometimes I feel pressure to perform well at school, but my father never seems to drive my sister to do better."
# doc5 = "Health experts say that Sugar is not good for your lifestyle."
#
# # compile documents
# doc_complete = [doc1, doc2, doc3, doc4, doc5]

logging.info('Compiling data ')
fonctions.progress(result_path0,4)

conn,cur=fonctions.get_connect(bdd_name)
doc_complete={}
for field in fields:
	results=cur.execute(" SELECT  id, data FROM " + dico_champs.get(field,field) )#+ ' limit 50 ')
	for res in results:
		doc_complete[res[0]]=doc_complete.get(res[0],' ') + res[1]


ids_complete=doc_complete.keys()
doc_complete=doc_complete.values()

###Then tune the vector space model###
if stopwordremoval:
	print 'language',language
	stop = set(stopwords.words(language))
else:
	stop={}
	
exclude = set(string.punctuation)

logging.info('Applying linguistic filters ')
logging.info('list of words in the stopwords list: ',', '.join(list(stop)))

fonctions.progress(result_path0,14)

def clean(doc):#),lowercase=True,stopwordremoval=True,punctuationfree=True,lemmatize=True):
	if lowercase:
		doc=doc.lower()
	if stopwordremoval:
		stop_free = " ".join([i for i in doc.split() if i not in stop])
	else:
		stop_free=" ".join([i for i in doc.split()])
	if punctuationfree:
		punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
	else:
		punc_free=stop_free
	if lemmatize:
		# if language=='english':
		#
		# 	lemma = WordNetLemmatizer()
		# 	normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
		# else:
			
			stemmer = SnowballStemmer(language)
			normalized = " ".join(stemmer.stem(word) for word in punc_free.split())
	else:
		normalized=punc_free
	return normalized

doc_clean = [clean(doc).split() for doc in doc_complete]

#print doc_clean

# Creating the term dictionary of our courpus, where every unique term is assigned an index.
dictionary = corpora.Dictionary(doc_clean)
#print "dictionary",dictionary


once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.iteritems() if docfreq < doc_threshold]


dictionary.filter_tokens(once_ids)


dictionary.filter_extremes(no_above=no_above)

dictionary.compactify() # remove gaps in id sequence after words that were removed



# Converting list of documents (corpus) into Document Term Matrix using dictionary prepared above.
doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean]

# Creating the object for LDA model using gensim library

from gensim.models.ldamulticore import LdaMulticore
Lda = LdaMulticore
# Running and Trainign LDA model on the document term matrix.
logging.info('Computing topics using gensim ')
fonctions.progress(result_path0,28)
#print iterations
def compute_coherence_values(dictionary, corpus, texts, limit, start=2, step=3):
    """
    Compute c_v coherence for various number of topics

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    texts : List of input texts
    limit : Max num of topics

    Returns:
    -------
    model_list : List of LDA topic models
    coherence_values : Coherence values corresponding to the LDA model with respective number of topics
    """
    coherence_values = []
    model_list = []
    print "range of possible topic numbers:",range(start, limit, step) 
    for num_topics in range(start, limit, step):
        logging.info('Testing with ' + str(num_topics) + ' topics.')
        print 'Testing with ' + str(num_topics) + ' topics.'
        #model = gensim.models.wrappers.LdaMallet(mallet_path, corpus=corpus, num_topics=num_topics, id2word=id2word)
        logging.getLogger().setLevel(logging.CRITICAL)
        try:
            model = Lda(corpus, num_topics=num_topics, iterations=iterations,id2word = dictionary, passes=50,alpha=alpha,workers=4)
            print ('using 4 workers')
        except:
            model = Lda(corpus, num_topics=num_topics, iterations=iterations,id2word = dictionary, passes=50,alpha=alpha)
            print ('using 1 worker')
        logging.getLogger().setLevel(logging.DEBUG)
        model_list.append(model)
        coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=dictionary, coherence='c_v')
        coherence_values.append(coherencemodel.get_coherence())
    return model_list, coherence_values

if topic_number>0:
	logging.getLogger().setLevel(logging.CRITICAL)
	try:
		ldamodel = Lda(doc_term_matrix, num_topics=topic_number, iterations=iterations,id2word = dictionary, passes=50,alpha=alpha,workers=4)
		print ('using 4 workers')
	except:
		ldamodel = Lda(doc_term_matrix, num_topics=topic_number, iterations=iterations,id2word = dictionary, passes=50,alpha=alpha)
		print ('using 1 worker')
	logging.getLogger().setLevel(logging.DEBUG)
	
else:
	froms=int(parameters_user.get('froms',2))
	
	steps=int(parameters_user.get('steps',6))
	maxs=min(int(parameters_user.get('tos',20)),200)+steps
	model_list, coherence_values = compute_coherence_values(dictionary=dictionary, corpus=doc_term_matrix, texts=doc_clean, start=froms, limit=maxs, step=steps)
	print "max coherence value",max(coherence_values),"in", coherence_values
	#optimal_model = model_list[]
	ldamodel = model_list[coherence_values.index(max(coherence_values))]#.show_topics(formatted=False)
		
	# Show graph
	x = range(froms, maxs, steps)
	plt.plot(x, coherence_values)
	plt.xlabel("Num Topics")
	plt.ylabel("Coherence score")
	plt.legend(("coherence_values"), loc='best')
	plt.savefig(os.path.join(result_path,"optimal_number_of_topics.pdf"))
	
	topic_number=range(froms, maxs, steps)[coherence_values.index(max(coherence_values))]
	logging.info('The optimal number of topics is ' + str(topic_number))
	print ('The optimal number of topics is ' + str(topic_number))
	
	
labels={}
print 'Topic composition:'
logging.info("Topic composition:")
for top in ldamodel.print_topics(num_topics=topic_number, num_words=30):
	logging.info(int(top[0])+1 , top[1])
	labels[top[0]] =str(int(top[0])+1)+'_'+'_'.join(map(lambda x:x.split('"')[1],top[1].split('+')[:5])) 
# Compute Perplexity
print('\nPerplexity: ', ldamodel.log_perplexity(doc_term_matrix))  # a measure of how good the model is. lower the better.

# Compute Coherence Score
coherence_model_lda = CoherenceModel(model=ldamodel, texts=doc_clean, dictionary=dictionary, coherence='c_v')
coherence_lda = coherence_model_lda.get_coherence()
print('\nCoherence Score: ', coherence_lda)


#print 'results'
#print(ldamodel.print_topics(num_topics=topic_number, num_words=10))

def get_top_most_topic(topic_rate_pair):
	trp = sorted(topic_rate_pair, key=lambda item: item[1], reverse=True)
	#print trp,clusterthreshold
	try:
		max_topics_per_document=int(parameters_user.get('max_topics_per_document',100))
	except:
		max_topics_per_document=100
	top_most_topic = [tr for tr in trp if tr[1]>clusterthreshold][:max_topics_per_document]
	
	return map(lambda x:x[0],top_most_topic),map(lambda x:int(x[1]*100),top_most_topic)

doc2topic = ldamodel[doc_term_matrix]
doc_topic_list = []
doc_topic_list_intensity=[]
topic_2doc={}
i=-1
for topic_rate_pair in doc2topic:
	i+=1
	#print 'topic_rate_pair',topic_rate_pair
	for topic_assignments in topic_rate_pair:
		topic_id=topic_assignments[0]
		topic_valence=topic_assignments[1]
		if not labels[topic_id] in topic_2doc:
			topic_2doc[labels[topic_id]]={}
		topic_2doc[labels[topic_id]][i]=topic_valence
	top_most_topic,top_most_topic_intensity = get_top_most_topic(topic_rate_pair)
	#print top_most_topic,top_most_topic_intensity
	#print top_most_topic
	doc_topic_list.append(map(lambda x:labels[x],top_most_topic))
	doc_topic_list_intensity.append(top_most_topic_intensity)

#print 'doc_topic_list',doc_topic_list

#print 'topic_2doc ',topic_2doc
#print 'ids_complete',ids_complete


logging.info('Drawing topic results using pyLDAvis: ')
fonctions.progress(result_path0,78)

c = doc_term_matrix#gensim.corpora.MmCorpus('corpus.mm')
data = pyLDAvis.gensim.prepare(ldamodel, doc_term_matrix, dictionary,mds='mmds',sort_topics=False)
finalviz=os.path.join(result_path,'vislda.html')
pyLDAvis.save_html(data,finalviz)
print finalviz, ' produced '

table_name='projection_cluster_LDA_'+customname+'_'.join(fields)+'_'+str(topic_number)
if len(custom_name)>0:
	table_name+='_'+custom_name.replace(' ','_').replace('-','_').replace('&','_').replace('(','_').replace(')','_').replace(':','_').replace(';','_').replace('.','_').replace('!','_').replace('é','e').replace('è','e').replace('ê','e').replace('à','a').replace('ô','o')
fonctions.create_table(table_name,conn,cur)
datasql = []
for doc_id, top_list in zip(ids_complete,doc_topic_list):
	#print doc_id,top_list
	#print 'top_list',top_list
	if len(top_list)>0:
		for top in top_list:
			datasql.append([filename,str(doc_id),str(top)])
if len(datasql)>0:
	fonctions.remplir_table_fast(bdd_name,table_name,datasql,' (file, id, data) ')

table_name='projection_cluster_LDA_intensity_'+customname+'_'.join(fields)+'_'+str(topic_number)
if len(custom_name)>0:
	table_name+='_'+custom_name.replace(' ','_').replace('-','_').replace('&','_').replace('(','_').replace(')','_').replace(':','_').replace(';','_').replace('.','_').replace('!','_').replace('é','e').replace('è','e').replace('ê','e').replace('à','a').replace('ô','o')
fonctions.create_table(table_name,conn,cur)
datasql = []
for doc_id, top_list in zip(ids_complete,doc_topic_list_intensity):
	#print doc_id,top_list
	if len(top_list)>0:
		for top in top_list:
			datasql.append([filename,str(doc_id),str(top)])
if len(datasql)>0:
	fonctions.remplir_table_fast(bdd_name,table_name,datasql,' (file, id, data) ')

#print ('ids_complete',ids_complete)
print "len(ids_complete)",len(ids_complete)



feed=open(os.path.join(result_path,'LDA_best_prototypes.txt'),'w')
for topic_id in topic_2doc:
	feed.write('\n\n\n**************************************\n')
	feed.write('**************************************\n')
	feed.write(topic_id)
	feed.write('**************************************\n')
	feed.write('**************************************\n')
	print 'topic_id',topic_id
	l=topic_2doc[topic_id].items()
	l.sort(key=itemgetter(1),reverse=True)
	#print l[:10]
	for x in l[:10]:
		#print 'x',x,ids_complete[x[0]]
		feed.write('# with probability ')
		feed.write(str(x[1]))
		feed.write('\n')
		feed.write(doc_complete[x[0]])
		feed.write('\n\n')
		
feed.close()


import re
p = re.compile("(-*\d+\.\d+) per-word .* (\d+\.\d+) perplexity")
matches = [p.findall(l) for l in open(os.path.join(result_path,'.user.log'))]
matches = [m for m in matches if len(m) > 0]
tuples = [t[0] for t in matches]
perplexity = [float(t[1]) for t in tuples]
liklihood = [float(t[0]) for t in tuples]
print "len(tuples)",len(tuples)
print 'tuples',tuples
iter = list(range(0,len(tuples)*10,10))
fig, ax1 = plt.subplots()

ax1.plot(iter,liklihood,c="blue",label='log likelihood')
ax1.set_ylabel('log likelihood', color='blue')
ax1.tick_params('y', colors='b')

ax2 = ax1.twinx()

ax2.plot(iter,perplexity,c="red",label='perplexity')
ax2.set_ylabel('perplexity', color='red')
ax2.tick_params('y', colors='r')
#plt.legend()#plt.ylabel("log liklihood")
#ax1.xlabel("iteration")
ax1.set_xlabel('iterations')

plt.title("Topic Model Convergence")
plt.grid()
plt.tight_layout()

plt.savefig(os.path.join(result_path,"convergence_liklihood.pdf"))
plt.close()



from librarypy import descriptor
#if not build_se:
schema_yaml={}
ix_directory=''
descriptor.generate(bdd_name,corpus_type,ix_directory=ix_directory,schema=schema_yaml)
from librarypy import descriptor
descriptor.generate(bdd_name)
logging.info('Topic Modeling ended successfully')
fonctions.progress(result_path0,100)
